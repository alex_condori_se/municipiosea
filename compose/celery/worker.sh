#!/bin/sh

set -o errexit
set -o nounset
set -o xtrace

celery -A config worker -l INFO