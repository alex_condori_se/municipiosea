#!/bin/sh
python manage.py migrate
python manage.py collectstatic --noinput -v0 -c
uwsgi --show-config --ini /app/uwsgi.ini
