# -*- encoding:utf-8 -*-
import environ
from os.path import join, dirname

import sys
from django.utils.translation import ugettext_lazy as _

DEBUG = True

PROJECT_PATH = dirname(dirname(dirname(__file__)))
APPS_PATH = join(PROJECT_PATH, "apps")

# DEBUG
# ------------------------------------------------------------------------------
env = environ.Env()
if not DEBUG:
    # TODO REMOVE FOR DOCKER
    env_file = join(PROJECT_PATH, ".environment")
    env.read_env(env_file)


# APP CONFIGURATION
# ------------------------------------------------------------------------------

ALLOWED_HOSTS = env.list('DJANGO_ALLOWED_HOSTS', default=['*'])
"""
CONSTANCE_BACKEND = 'constance.backends.database.DatabaseBackend'
CONSTANCE_DATABASE_PREFIX = 'constance:municipiosea:'
CONSTANCE_CONFIG = {
    'THE_ANSWER': (42, 'Answer to the Ultimate Question of Life, '
                       'The Universe, and Everything'),
}
"""
DJANGO_APPS = [
    # Default Django apps:
    "django.contrib.auth",

    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.redirects",
    "django.contrib.staticfiles",
    "django.contrib.postgres",

    # Useful template tags:
    # "django.contrib.humanize",
    "django.contrib.admin",

]

THIRD_PARTY_APPS = [

    "crispy_forms",  # Form layouts
    "allauth",  # registration
    "allauth.account",  # registration

    #"allauth.socialaccount",  # registration
    #"allauth.socialaccount.providers.facebook",
    #"allauth.socialaccount.providers.google",

    # API Rest
    "rest_framework",
    "rest_framework.authtoken",
    "corsheaders",

    # Assets
    "imagekit",
    "compressor",

    # Settings
    "constance",

    # Django location field
    "location_field.apps.DefaultConfig",

    # Tasks Management
    # "django_celery_beat",
    # "django_celery_results",


    'easy_thumbnails',
    #'constance.backends.database',
    'picklefield',
]

LOCAL_APPS = [
    'apps.contrib',
    'apps.users',
    'apps.auths',
]

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS


# MIDDLEWARE CONFIGURATION
# ------------------------------------------------------------------------------
MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.contrib.redirects.middleware.RedirectFallbackMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
]
# MIGRATIONS CONFIGURATION
# ------------------------------------------------------------------------------
MIGRATION_MODULES = {
    'sites': 'apps.contrib.sites.migrations'
}

# FIXTURE CONFIGURATION
# ------------------------------------------------------------------------------
FIXTURE_DIRS = (
    join(APPS_PATH, "fixtures"),
)
# EMAIL CONFIGURATION
# ------------------------------------------------------------------------------
EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND', default='django.core.mail.backends.console.EmailBackend')
EMAIL_HOST, EMAIL_PORT = '127.0.0.1', 1025  # Work with MailHog
DEFAULT_FROM_EMAIL = env('DEFAULT_FROM_EMAIL', default='Support <support@xiberty.com>')
#DEFAULT_FROM_EMAIL = env('DEFAULT_FROM_EMAIL', default='Support <support@xiberty.com>')

# MANAGER CONFIGURATION
# ------------------------------------------------------------------------------
ADMINS = (
    ("Support", 'support@xiberty.com'),
)
MANAGERS = ADMINS

# DATABASE CONFIGURATION
# ------------------------------------------------------------------------------
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'municipiosea_app',
        'USER': 'municipiosea_user',
        'PASSWORD': 'postgres',
        'HOST': 'localhost',
        'PORT': '',

    }
}
DATABASES['default']['ATOMIC_REQUESTS'] = True



# GENERAL CONFIGURATION
# ------------------------------------------------------------------------------
TIME_ZONE = 'America/La_Paz'
LANGUAGE_CODE = 'es-BO'
SITE_ID = 1
USE_I18N = True
USE_L10N = True
USE_TZ = True


# TEMPLATE CONFIGURATION
# ------------------------------------------------------------------------------
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            join(APPS_PATH, 'templates'),
        ],
        'OPTIONS': {
            'debug': DEBUG,
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',

                'constance.context_processors.config',

                # Custom context processor
                'apps.contrib.context_processors.website',

                #"allauth.account.context_processors.account",
                #"allauth.socialaccount.context_processors.socialaccount",
            ],

        },
    },
]

# See: http://django-crispy-forms.readthedocs.io/en/latest/install.html#template-packs
# STATIC FILE CONFIGURATION
# ------------------------------------------------------------------------------
#STATIC_ROOT = join(PROJECT_PATH, 'assets')
STATIC_ROOT = 'municipiosea/apps/assets'
STATIC_URL = '/assets/'
print('static_root',STATIC_ROOT,'static_url: ',STATIC_URL)

STATICFILES_DIRS = (
    join(APPS_PATH, 'assets'),
    #"/home/municipiosea/apps/assets",
)
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',

    # Compressor
    'compressor.finders.CompressorFinder',
)

# MEDIA CONFIGURATION
# ------------------------------------------------------------------------------
MEDIA_ROOT = join(PROJECT_PATH, 'public/media')
MEDIA_URL = '/media/'

print ("MEDIA_ROOT: ",MEDIA_ROOT,"MEDIA_URL: ",MEDIA_URL)

# URL Configuration
# ------------------------------------------------------------------------------
ROOT_URLCONF = 'config.urls'
#WSGI_APPLICATION = 'config.wsgi.application'

# PASSWORD VALIDATION
# ------------------------------------------------------------------------------
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 6,
        }
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# AUTHENTICATION CONFIGURATION
# ------------------------------------------------------------------------------
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)

# SLUGLIFIER
AUTOSLUG_SLUGIFY_FUNCTION = 'slugify.slugify'


# PLATFORM SETTINGS
# ------------------------------------------------------------------------------
PROJECT_NAME = "municipiosea"
PROJECT_DOMAIN = "https://municipiosea.com"
PROJECT_AUTHOR = "Xiberty <alex.condori@xiberty.com>"
PROJECT_DESCRIPTION = "Django 2.0 template with steroids"
PROJECT_SUPPORT_EMAIL = "support@xiberty.com"
PROJECT_SUPPORT_PHONE = "(+591) 72529356, (+591) 73234523"


SERVER_URL = env.str('DJANGO_SERVER_URL', default=PROJECT_DOMAIN)
TOKEN_EXPIRATION_DAYS = env.int('DJANGO_TOKEN_EXPIRATION_DAYS', default=7)
MANAGE_TRANSACTIONS = env.bool('DJANGO_MANAGE_TRANSACTIONS', default=True)

"""
CONSTANCE_CONFIG = {
    'REGISTRATION_OPEN': (True, _("Esta variable determina si el registro de cuentas nuevas esta activo.")),
    'SOCIAL_REGISTRATION_OPEN': (True, _("Esta variable determina si el registro de cuentas mediante redes sociales esta activo.")),
    'ADDRESS_XIBERTY': ('Cerca de la plaza Jose Marti No. 1881',_("Dirección de la empresa")),
    'CITY_XIBERTY': ('San Pedro - La Paz',_("Ciudad de la empresa")),
    'PHONE_XIBERTY': ('(+591) 73234523 (+591) 78805253',_("Telefono de la empresa")),
    'EMAIL_XIBERTY': ('info@xiberty.com',_("Correo Elecrónico de la empresa")),
}

CONSTANCE_CONFIG_FIELDSETS = {
    'General': ('REGISTRATION_OPEN', 'SOCIAL_REGISTRATION_OPEN','ADDRESS_XIBERTY','CITY_XIBERTY','PHONE_XIBERTY','EMAIL_XIBERTY'),
}
"""
#CONSTANCE_BACKEND = 'constance.backends.redisd.RedisBackend'
#CONSTANCE_REDIS_CONNECTION = env('REDIS_CACHE_URL', default='redis://redis:6379/0')
#CONSTANCE_IGNORE_ADMIN_VERSION_CHECK = True
#PAGINATION_PAGE_SIZE = 12


# ADMIN SETTINGS
# ------------------------------------------------------------------------------
ADMIN_URL = r'^superadmin/'


# ALLAUTH CONFIGURATION
# ------------------------------------------------------------------------------
# Some really nice defaults
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_REQUIRED = True
#ACCOUNT_EMAIL_VERIFICATION = 'mandatory'  # mandatory, optional, none
ACCOUNT_ALLOW_REGISTRATION = env.bool('DJANGO_ACCOUNT_ALLOW_REGISTRATION', True)
ACCOUNT_UNIQUE_EMAIL = True
ACCOUNT_LOGOUT_ON_PASSWORD_CHANGE = False
ACCOUNT_LOGIN_ON_PASSWORD_RESET = False
ACCOUNT_LOGIN_ON_EMAIL_CONFIRMATION = True
ACCOUNT_SESSION_REMEMBER = True
ACCOUNT_USERNAME_BLACKLIST = ["axel", "admin"]

ACCOUNT_ADAPTER = 'apps.users.adapters.AccountAdapter'
#SOCIALACCOUNT_ADAPTER = 'apps.users.adapters.SocialAccountAdapter'

AUTH_USER_MODEL = 'users.User'
#LOGIN_REDIRECT_URL = '/admin/dashboard/'
#LOGIN_REDIRECT_URL = "/"
#LOGIN_URL = 'account_login'
ACCOUNT_AUTHENTICATED_LOGIN_REDIRECTS = True
ACCOUNT_FORMS = {
    'signup': 'apps.users.forms.SignupForm',
    'login' : 'allauth.account.forms.LoginForm'
}
ACCOUNT_SIGNUP_PASSWORD_ENTER_TWICE = False
ACCOUNT_USERNAME_REQUIRED = False

"""
SOCIALACCOUNT_PROVIDERS = {
    'facebook': {
        'METHOD': 'oauth2',
        'SCOPE': ['email', 'public_profile'],
        'AUTH_PARAMS': {'auth_type': 'https'},
        # 'AUTH_PARAMS': {'auth_type': 'reauthenticate'},
        'FIELDS': [
            'id',
            'email',
            'name',
            'first_name',
            'last_name',
            'verified',
            'locale',
            'timezone',
            'link',
            'gender',
            'updated_time'],
        'EXCHANGE_TOKEN': True,
        'LOCALE_FUNC': lambda request: 'es_BO',
        'VERIFIED_EMAIL': False,
        'VERSION': 'v2.11'
    },
    'google': {
        'SCOPE': [
            'profile',
            'email',
        ],
        'AUTH_PARAMS': {
            'access_type': 'online',
        }
    }
}
"""

# REST FRAMEWORK CONFIGURATION
# ------------------------------------------------------------------------------
#OAUTH2_PROVIDER_APPLICATION_MODEL = 'credentials.PlatformApp'
CORS_ORIGIN_ALLOW_ALL = True

REST_FRAMEWORK = {

    'EXCEPTION_HANDLER': 'apps.contrib.api.exceptions.formatted_exception_handler',
    'DEFAULT_METADATA_CLASS': 'rest_framework.metadata.SimpleMetadata',
    'DEFAULT_RENDERER_CLASSES': (
        'apps.contrib.renderers.SafeJSONRenderer',
    ),

    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.TokenAuthentication',
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ],

    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.JSONParser',
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser',
        'rest_framework.parsers.FileUploadParser',
    ),

    'TEST_REQUEST_RENDERER_CLASSES': (
        'rest_framework.renderers.MultiPartRenderer',
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.TemplateHTMLRenderer'
    ),

    'DEFAULT_PAGINATION_CLASS': 'apps.contrib.api.pagination.LinkHeaderPagination',
    'PAGE_SIZE': 100,
    'DEFAULT_VERSIONING_CLASS': 'rest_framework.versioning.URLPathVersioning',
    'TEST_REQUEST_DEFAULT_FORMAT': 'json',
}
APPEND_SLASH = False

# LOGGING SETTINGS
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '%s/django.log' % PROJECT_PATH,
        },
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'propagate': True,
            'level': 'INFO',
        },
        'django.request': {
            # 'handlers': ['mail_admins'],
            'handlers': ['file', 'console'],
            'level': 'ERROR',
            'propagate': False,
        },
    }
}

"""
# CACHE CONFIGURATION
# ------------------------------------------------------------------------------
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": env('REDIS_CACHE_URL', default='redis://redis:6379/1'),
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

# TASK MANAGEMENT CONFIGURATION
# ------------------------------------------------------------------------------
CELERY_BROKER_URL = env('CELERY_BROKER_URL', default='redis://redis:6379/1')
CELERY_RESULT_BACKEND = env('CELERY_BROKER_URL', default='redis://redis:6379/1')
CELERY_TIMEZONE = TIME_ZONE
"""


# CKEDITOR
CKEDITOR_JQUERY_URL = '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'
CKEDITOR_UPLOAD_PATH = "uploads/"
CKEDITOR_BROWSE_SHOW_DIRS = True


# GENERAL CONFIGURATION
# ------------------------------------------------------------------------------
DATA_UPLOAD_MAX_NUMBER_FIELDS = 5000

# DJANGO LOCATION FIELD CONFIGURATIONS
# ------------------------------------------------------------------------------

LOCATION_FIELD_PATH = STATIC_URL + 'location_field'
LOCATION_FIELD = {
    'map.provider': 'google',
    'map.zoom': 13,

    'search.provider': 'google',
    'search.suffix': '',

    # Google
    'provider.google.api': '//maps.google.com/maps/api/js?sensor=false',
    'provider.google.api_key': 'AIzaSyDTxdGj9eWwtE6Kbg7k1psAZ81aEc5RO8c',
    'provider.google.api_libraries': '',
    'provider.google.map.type': 'ROADMAP',

    # Mapbox
    'provider.mapbox.access_token': '',
    'provider.mapbox.max_zoom': 18,
    'provider.mapbox.id': 'mapbox.streets',

    # OpenStreetMap
    'provider.openstreetmap.max_zoom': 18,

    # misc
    'resources.root_path': LOCATION_FIELD_PATH,
    'resources.media': {
        'js': (
            LOCATION_FIELD_PATH + '/js/jquery.livequery.js',
            LOCATION_FIELD_PATH + '/js/form.js',
        ),
    },
}


# DISQUS
DISQUS_KEY = 'SSgMGVqsA6dPuW1QRPRLJr0LvOYJB9HjJRqfPbIJ23HTKkWsfG7Njp5qREsLmOnc'
DISQUS_API_KEY = 'SSgMGVqsA6dPuW1QRPRLJr0LvOYJB9HjJRqfPbIJ23HTKkWsfG7Njp5qREsLmOnc'
DISQUS_SECRET = 'GIRamGOOIUrzaIN2oIKdVdHhwIEc8jkjHxwiyh2JlN6s5n8EYOnWnXRiVco9kC8V'
DISQUS_WEBSITE_SHORTNAME = 'municipiosea'


