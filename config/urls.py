# -*- encoding:utf-8 -*-

from __future__ import unicode_literals

from django.conf import settings
from django.urls import include, path
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from django.views import defaults as default_views

urlpatterns = [

    # Django ADMIN
    path('superadmin/', admin.site.urls),

    # Inicio
    #path('', TemplateView.as_view(template_name="reyes/index-charity.html")),
    path('', TemplateView.as_view(template_name="landing.html")),
    #path('', TemplateView.as_view(template_name="planwi/home.html"), name="landing"),

    # Usuarios
    #path('',  include("apps.users.urls", namespace="users")), # vistas
    #path('api', include("apps.users.api.urls", namespace="api-users")), # api

    # Cuentas
    #path('account/', include('allauth.urls')), # Vistas
    #path('transactions/', include('apps.auths.urls')), # Vistas de transaccion
    #path('api', include('apps.auths.api.urls', namespace="api-auth")), # api

    # EVENTS
    #path('', include('apps.events.urls', namespace="events")),
    #path('api-events', include('apps.events.api.urls', namespace="api-events")), # api

    # Pluggins
    #path('tinymce/', include('tinymce.urls')),

    # Loggin
    # path('acceder/', TemplateView.as_view(template_name="account/login.html"), name="acceder"),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if settings.DEBUG:

    # Correos electronicos
    """
    urlpatterns += [
        path('email/base/', TemplateView.as_view(
            template_name='emails/base.html')),

        path('email/web/confirmation/', TemplateView.as_view(
            template_name='emails/account/email_confirmation_signup/message.html')),

        path('email/web/password-reset/', TemplateView.as_view(
            template_name='emails/account/password_reset/message.html')),

        path('email/web/welcome/', TemplateView.as_view(
            template_name='emails/account/welcome/message.html')),
    ]
    """

    # Paginas de errores
    """
    urlpatterns += [
        path('400/', TemplateView.as_view(template_name='errors/400.html')),
        path('403/', TemplateView.as_view(template_name='errors/403.html')),
        path('404/', TemplateView.as_view(template_name='errors/404.html')),
        path('500/', TemplateView.as_view(template_name='errors/500.html')),
    ]

    urlpatterns += [
        path('error/400/', default_views.bad_request, kwargs={'exception': Exception('Bad Request!')}),
        path('error/403/', default_views.permission_denied, kwargs={'exception': Exception('Permission Denied')}),
        path('error/404/', default_views.page_not_found, kwargs={'exception': Exception('Page not Found')}),
        path('error/500/', default_views.server_error),
    ]
    """

    """
    # Herramientas para desarrollo
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar
        urlpatterns += [
            path('__debug__/', include(debug_toolbar.urls)),
        ]
else:
    handler400 = TemplateView.as_view(template_name="errors/400.html")
    handler403 = TemplateView.as_view(template_name="errors/403.html")
    handler404 = TemplateView.as_view(template_name="errors/404.html")
    handler500 = TemplateView.as_view(template_name="errors/500.html")
    """
