
FROM python:3.5
ENV PYTHONUNBUFFERED 1


RUN groupadd -r django \
    && useradd -r -g django django

COPY ./requirements /requirements

RUN pip install -r /requirements/production.txt

RUN apt-get update && apt-get -y install postgresql postgresql-contrib
RUN apt-get update && apt-get -y install -y postgresql-client

COPY ./compose/django/entrypoint.sh /entrypoint.sh
COPY ./compose/django/start.sh /start.sh


RUN sed -i 's/\r//' /entrypoint.sh \
    && sed -i 's/\r//' /start.sh \
    && chmod +x /entrypoint.sh \
    && chown django /entrypoint.sh \
    && chmod +x /start.sh \
    && chown django /start.sh


COPY . /app
RUN chown -R django /app

USER django
WORKDIR /app

ENTRYPOINT "/bin/sh", "/entrypoint.sh"]
CMD ["/bin/sh", "/start.sh"]

