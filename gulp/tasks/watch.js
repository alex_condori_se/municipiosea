var gulp = require('gulp');
var sass = require('gulp-sass');
var build = require('./build');

gulp.task('watch', function () {
    gulp.watch([build.config.path.src + '/scss/**/*.scss', build.config.path.src + '/scripts/**/*.js'], ['bundle']);
});

gulp.task('watch:scss', function () {
    gulp.watch(build.config.path.src + '/scss/**/*.scss', ['bundle']);
});

gulp.task('watch:js', function () {
    gulp.watch(build.config.path.src + '/scripts/**/*.js', ['bundle']);
});