import json

from django.test.client import Client
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase, APIClient, RequestsClient
from rest_framework.authtoken.models import Token
from rest_framework_extensions.test import APIRequestFactory

from apps.auths.models import UserAction
from apps.users.models import User


class UsersAPITests(APITestCase):

    def setUp(self):
        """
            1. Crea un nuevo usuario
        """
        User.objects.create(username='test', email='test@test.com', password='12345678x')
        self.client = APIClient()

    def login(self):
        url = reverse('api-auth:v1:login')
        resp = self.client.post(url, data={
            'login': 'test',
            'password': '12345678x'
        })
        return resp.json()



    # def test_login2(self):
    #     """
    #        POST /api/v1/auth/login/
    #     """
    #     #  Obtiene la url del WS
    #     url = reverse('api-auth:v1:login')
    #     resp = self.client.post(url, data={'login': 'test', 'password': '12345678x'})
    #
    #     #  Verifica que sea una respuesta valida
    #     self.assertEqual(resp.status_code, status.HTTP_200_OK)
    #     response = resp.json()
    #     token = Token.objects.get(user=self.user).key
    #     answer = {'profile': UserSerializer(self.user).data, 'token': token}
    #
    #     #  Verifica que la informacion que se recibe sea la que se espera
    #     self.assertEqual(response, answer)


    def test_login(self):
        """
           POST /api/v1/auth/login/
        """
        user = self.login()
        print(user)

        answer = user

        self.assertEqual(user, answer)

    def test_register(self):
        """
           POST /api/auth/register/
        """

        url = reverse('api-auth:v1:register')
        data = {
            "client_id": "bgzxrqgTnmCzHxq5CJTusph5g7WpvhQmHLGQQehH",
            "client_secret": "L7B0YRVoIy62a8PYCZc2r3VGPQscujFW6nwV97WV7UpVlMNVHEsV5PIyCs7bWnEGZV3XKZhoTYjq6fysMWRNhTq8aSBCjFBjwW6v57ccXViWmu0DeTC7111aNlHa0XPU",
            "username": "vicccs",
            "email": "assd@as.com",
            "password1": "rampagegear",
            "password2": "rampagegear"}

        resp = self.client.post(url, data=data)
        response = resp.json()
        print(response)

        answer = {'code': 200, 'message': 'Confirmación de correo electrónico enviado'}

        self.assertEqual(response, answer)

        # Segundo intento, con datos duplicados

        resp = self.client.post(url, data=data)
        response = resp.json()

        answer = {"message": "El correo electrǿnico ya esta siendo utilizado.", "code": "unavailable_email"}

        self.assertEqual(response, answer)

    def test_logout(self):
        """
           POST /api/v1/auth/logout/
        """

        # Login
        self.login()
        url = reverse('api-auth:v1:logout')
        token = Token.objects.all()[0]

        request = self.client.post(url, {"token": token.key})

        answer = {'code': 200, 'message': 'Se ha cerrado sesión exitosamente.'}

        self.assertEqual(request.data, answer)

    def reset_pass(self):
        user = self.login()
        url = reverse('api-auth:v1:password-reset')
        return self.client.post(url, {"login": user['profile']['email']})


    def test_reset_password(self):
        """
           POST /api/v1/auth/logout/
        """
        request = self.reset_pass()

        answer = {
            "message": "Un correo para restaurar contraseña fué enviado",
            "code": 200
        }

        self.assertEqual(request.data, answer)

    def test_reset_password_confirm(self):
        """
           POST /api/v1/auth/logout/
        """

        # Login
        user = self.login()
        self.reset_pass()
        url = reverse('api-auth:v1:password-reset-confirm')
        # import ipdb; ipdb.set_trace()
        user_action = UserAction.objects.all()[0]

        request = self.client.post(url, {
            "token": user_action.token,
            "password1": "12345678x",
            "password2": "12345678x",
        })

        answer = {
            "code": 200,
            "message": "Contraseña Actualizada"
        }
        print(request.data)

        self.assertEqual(request.data, answer)

    def test_get_profile(self):
        """
           URL:
               - GET /api/me/profile/
        """
        user = self.login()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + user['token'])
        url = reverse('api-users:v1:profile')
        request = self.client.get(url)

        answer = {'last_name': user['profile']['last_name'],
                  'email': user['profile']['email'],
                  'id': user['profile']['id'],
                  'first_name': user['profile']['first_name'],
                  'username': user['profile']['username'],
                  'photo': user['profile']['photo'],
                  'has_usable_password': True
                  }

        self.assertEqual(request.data, answer)

    def test_set_password(self):
        """
           POST /api/v1/auth/logout/
        """

        # Login
        user = self.login()
        self.reset_pass()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + user['token'])
        url = reverse('api-users:v1:password')
        user_action = UserAction.objects.all()[0]

        request = self.client.post(url, {
            "token": user_action.token,
            "password": "12345678x",
            "confirm_password": "12345678x",
        })

        answer = user['profile']
        print(request.data)

        self.assertEqual(request.data, answer)

    def _test_update_profile(self):
        """
           URL:
               - PUT /api/me/profile/
        """

        # Login
        user = self.login()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + user['token'])
        url = reverse('api-users:v1:profile')

        data = {"first_name": "test first name",
                "last_name": "test last name",
                "username": "test",
                "photo": None}

        request = self.client.put(url, data, content_type='application/json')

        answer = user['profile']

        self.assertEqual(request.data, answer)

    def test_change_password(self):
        """
           POST /api/v1/auth/logout/
        """

        # Login
        user = self.login()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + user['token'])

        url = reverse('api-users:v1:password')

        request = self.client.put(url, {
            "password": "12345678x",
            "password1": "12345678x",
            "password2": "12345678x",
        })

        answer = user['profile']
        print(request.data)

        self.assertEqual(request.data, answer)

    def test_change_email(self):
        """
           POST /api/v1/auth/logout/
        """

        # Login
        user = self.login()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + user['token'])
        url = reverse('api-users:v1:email')

        new_email = "test222@mail.com"

        request = self.client.put(url, {
            "password": "12345678x",
            "new_email": new_email,
            "token": "",
        })

        user['profile']['email'] = new_email

        answer = user['profile']
        print(request.data)

        self.assertEqual(request.data, answer)
