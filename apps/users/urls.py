# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.urls import path, re_path, include

from apps.users.views import ProfileView#, DashboardView
#from apps.events.views import DashboardView

app_name = 'users'
urlpatterns = [
    #re_path('admin/', DashboardView.as_view(), name='dashboard'),
    path('admin/perfil/', ProfileView.as_view(), name='view-profile'),
    #path('account/', include('allauth.urls'), name="login"),
]
