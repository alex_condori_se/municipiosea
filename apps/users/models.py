# -*- encoding:utf-8 -*-

from __future__ import unicode_literals, absolute_import

from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.core import validators
from django.db import models
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill

from apps.contrib.utils.files import get_file_path

REQUIRED_FIELDS = getattr(settings, 'PROFILE_REQUIRED_FIELDS', ['email'])


class User(AbstractUser):

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = REQUIRED_FIELDS

    username = models.CharField(
        _('username'),
        max_length=30,
        unique=True,
        help_text=_('Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[
            validators.RegexValidator(
                r'^[\w.@+-]+$',
                _('Enter a valid username. This value may contain only '
                  'letters, numbers ' 'and @/./+/-/_ characters.')
            ),
        ],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
        db_index=True,
    )

    email = models.EmailField(
        _('email address'),
        error_messages={
            'unique': _("A user with that email already exists."),
        },
        unique=True,
        db_index=True,
    )

    photo = ProcessedImageField(
        verbose_name=_('Photo'),
        upload_to='users/%Y/%m/%d',
        processors=[ResizeToFill(350, 350)],
        format='PNG',
        options={'quality': 80},
        blank=True,
        null=True,
    )

    first_name = models.CharField(
        verbose_name=_('Nombres'),
        max_length=100, blank=True, null=True,
    )

    last_name = models.CharField(
        verbose_name=_('Apellidos'),
        max_length=150, blank=True, null=True,
    )

    def __str__(self):
        return self.username

    def save(self, *args, **kwargs):
        if not self.pk and self.has_usable_password() is False:
            self.set_password(self.password)
        super(User, self).save(*args, **kwargs)


    def change_password(self, new_password):
        self.set_password(new_password)
        self.save()

    @property
    def full_name(self):
        if self.first_name and self.last_name:
            return '%s %s' % (self.first_name, self.last_name)
        return self.username

    @property
    def avatar(self):
        return get_file_url(self.photo) if self.photo else None

    @property
    def allOrganizations(self):
        return self.owned_organizations.all().order_by('-id')

    @property
    def likes(self):
        return self.event_likes.all().count()

    @property
    def allOrganizationsMemberships(self):
        return self.user_membership.all()

    @property
    def allMemberships(self):
        return self.user_membership.all()

    @property
    def get_likes(self):
        a = self.event_likes.all().count()
        return a

    def has_owned_organizations(self):
        print(self.owned_organizations.all(),"**********")
        if hasattr(self, "owned_organizations") and len(self.owned_organizations.all()) > 0:
            return True
        return False

    def has_memberships(self):
        if hasattr(self, "user_membership") and len(self.user_membership.all()) > 0:
            return True
        return False

    def has_organization_session(self):
        if hasattr(self, "organization_session") and self.organization_session:
            return True
        return False

    def get_organization(self):
        _organizations = []
        own = self.has_owned_organizations()
        if own:
            for organization in self.owned_organizations.all():
                _organizations.append(organization)
        ship = self.has_memberships()
        if self.has_memberships():
            for membership in self.user_membership.all():
                _organizations.append(membership.organization)
        return _organizations
#probando membership
    def get_membership_user(self):
        _memberships = []
        ship = self.has_memberships()
        if ship:
            #print("existe membership")
            for membership in self.user_membership.all():
                _memberships.append(membership)
        return _memberships

    @property
    def get_users(self):
        #hacer que solo envie los usuarios que no estan en memberships
        #members = Membership.objects.all()
        #member = self.get_membership()
        users = User.objects.filter()
        res = User.objects.all()
        return res

    @cached_property
    def managed_organization(self):
        return self.get_organization()

    def get_organization_contract(self, organization):
        #import ipdb; ipdb.set_trace()
        return self.user_membership.get(organization=organization)

    def registry(self, organization):
        now_date = str(timezone.now().date())
        if hasattr(self, "registries") and len(self.registries.all()) > 0:
            if self.registries.all().filter(organization=organization, day=now_date).exists():
                return self.registries.all().get(organization=organization, day=now_date)
        return None

    def status(self, organization):
        registry = self.registry(organization)
        if registry:
            if len(registry.data) > 0:
                last = registry.data[len(registry.data)-1]
                return last["action"]
        return "checkout"


    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')
        app_label = 'users'
