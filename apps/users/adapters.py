# -*- encoding:utf-8 -*-

import hashlib
from urllib.request import urlopen

from allauth.account.adapter import DefaultAccountAdapter
from allauth.account.utils import perform_login
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.urls import reverse

from apps.contrib.context_processors import GLOBAL_CONTEXT


UserModel = get_user_model()

class AccountAdapter(DefaultAccountAdapter):
    def is_open_for_signup(self, request):
        return getattr(settings, 'ACCOUNT_ALLOW_REGISTRATION', True)

    def new_user(self, request):
        return super(AccountAdapter, self).new_user(request)

    def populate_username(self, request, user):
        return super(AccountAdapter, self).populate_username(request, user)

    def confirm_email(self, request, email_address):
        return super(AccountAdapter, self).confirm_email(request, email_address)

    def generate_unique_username(self, txts, regex=None):
        return super(AccountAdapter, self).generate_unique_username(txts, regex)

    def get_login_redirect_url(self, request):
        return super(AccountAdapter, self).get_login_redirect_url(request)


    def send_mail(self, template_prefix, email, context):
        _context =  {**context, **GLOBAL_CONTEXT}
        return super(AccountAdapter, self).send_mail(template_prefix, email, _context)


class SocialAccountAdapter(DefaultSocialAccountAdapter):

    def pre_social_login(self, request, sociallogin):
        user = sociallogin.user
        if not user.id:
            if UserModel.objects.filter(email=user.email).exists():
                customer = UserModel.objects.get(email=user.email)  # if user exists, connect the account to the existing account and login
                sociallogin.state['process'] = 'connect'
                perform_login(request, customer, 'none')
        return


    def is_open_for_signup(self, request, sociallogin):
        return getattr(settings, 'ACCOUNT_ALLOW_REGISTRATION', True)

    def new_user(self, request, sociallogin):
        return super(SocialAccountAdapter, self).new_user(request, sociallogin)

    def save_user(self, request, sociallogin, form=None):
        user = super(SocialAccountAdapter, self).save_user(request, sociallogin, form)
        avatar_size = 256
        picture_url = "http://www.gravatar.com/avatar/{0}?s={1}".format(
            hashlib.md5(user.email.encode('UTF-8')).hexdigest(),
            avatar_size
        )

        if sociallogin.account.provider == 'facebook':
            f_name = sociallogin.account.extra_data['first_name']
            l_name = sociallogin.account.extra_data['last_name']
            if f_name:
                user.first_name = f_name
            if l_name:
                user.last_name = l_name

            # verified = sociallogin.account.extra_data['verified']

            picture_url = "http://graph.facebook.com/{0}/picture?width={1}&height={1}".format(
                sociallogin.account.uid, avatar_size)

        image_url = picture_url
        img_temp = NamedTemporaryFile(delete=True)
        img_temp.write(urlopen(image_url).read())
        img_temp.flush()

        user.photo.save("photo_user_%s.png" % user.pk, File(img_temp))
        user.password = ""
        user.save()

        return user

    def populate_user(self, request, sociallogin, data):
        return super(SocialAccountAdapter, self).populate_user(request, sociallogin, data)


    # def get_connect_redirect_url(self, request, socialaccount):
    #     print("\n\n\n\n CUENTA SOCIAL  \n\n\n\n")
    #     return reverse("user:messages")


