from django.utils.safestring import mark_safe

from apps.users.api.v1.serializers import UserSerializer


class UserJSONMixin(object):

    def get_context_data(self, **kwargs):
        context = super(UserJSONMixin, self).get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            context['user_json'] = mark_safe(UserSerializer(self.request.user).data)
        return context
