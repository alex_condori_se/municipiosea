# -*- encoding:utf-8 -*-

from django.contrib.auth import update_session_auth_hash, get_user_model
from django.db.models.query_utils import Q
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from rest_framework.viewsets import GenericViewSet

from apps.contrib.api.responses import DoneResponse
from apps.contrib.api.viewsets import PermissionViewSet
from apps.users.api.v1 import codes
from apps.users.api.v1.serializers import SearchUserSerializer, PasswordSetSerializer
from apps.users.models import User
from .messages import UserMessages

from .serializers import (
    UserSerializer,
    UserUpdateSerializer,
    PasswordUpdateSerializer,
    EmailUpdateSerializer,
)

UserModel = get_user_model()


class AccountViewSet(PermissionViewSet):

    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated,]
    permissions_by_action = {
        "cancel": [IsAuthenticated],
    }



    def get_profile(self, request):
        """
        Get the user profile.
        """
        return Response(UserSerializer(request.user).data,
                        status=status.HTTP_200_OK)

    def update_profile(self, request, *args, **kwargs):
        """
        Update de user profile info
        """
        serializer = UserUpdateSerializer(
            data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = request.user
        #import ipdb; ipdb.set_trace()

        if "username" in serializer.data:
            user.username = serializer.data["username"]

        if "first_name" in serializer.data:
            user.first_name = serializer.data["first_name"]
        if "last_name" in serializer.data:
            user.last_name = serializer.data["last_name"]
        if "photo" in serializer.data:
            ph = request.data["photo"]
            user.photo.save(ph.name, ph)
        user.save()
        return Response(UserSerializer(user).data,
                        status=status.HTTP_201_CREATED)

    def set_password(self, request):
        serializer = PasswordSetSerializer(
            data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        request.user.set_password(serializer.validated_data["password"])
        request.user.save()

        return Response(UserSerializer(request.user).data,
                        status=status.HTTP_201_CREATED)

    def update_password(self, request):
        """
        Change the password of the current user.
        """
        serializer = PasswordUpdateSerializer(
            data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        request.user.set_password(serializer.validated_data["password1"])
        update_session_auth_hash(request, request.user)
        request.user.save()
        return Response(UserSerializer(request.user).data,
                        status=status.HTTP_201_CREATED)

    def update_email(self, request):
        """
        Change the current user email change.
        """
        serializer = EmailUpdateSerializer(
            data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = request.user
        user.email = serializer.data["new_email"]
        user.save()

        return Response(UserSerializer(user).data,
                        status=status.HTTP_201_CREATED)

    def cancel(self, request):
        """
        Init the Cancel account flow and send token for it.
        """
        UserMessages.cancel_sent(request)
        return DoneResponse(**codes.ACCOUNT_CANCELLED)






class UserList(ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = SearchUserSerializer
    queryset = User.objects.all()

    def get_queryset(self):
        qs = self.queryset
        query = self.request.GET.get("q", None)
        if query:
            qs = qs.filter(
                Q(username__contains=query) | Q(first_name__contains=query) | Q(last_name__contains=query)
            )
        return qs

