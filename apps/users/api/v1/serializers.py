# -*- encoding:utf-8 -*-
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from apps.users.api.v1 import codes
from apps.contrib.api.exceptions import ValidationError

UserModel = get_user_model()
PASSWORD_MAX_LENGTH = UserModel._meta.get_field('password').max_length
user_read_only_fields = (
    'id', 'username', 'date_joined', 'last_login', 'new_email',
    'password', 'is_superuser', 'is_staff', 'is_active', 'date_joined',
    'email_token', 'token', 'groups', 'user_permissions',
)


class UserSerializer(serializers.ModelSerializer):
    has_usable_password = serializers.SerializerMethodField()
    photo = serializers.SerializerMethodField()

    def get_has_usable_password(self, obj):
        return obj.has_usable_password()

    def get_photo(self, obj):
        if obj.photo:
            return settings.PROJECT_DOMAIN + obj.photo.url
        return ""

    class Meta:
        model = UserModel
        fields = [
            'username', 'first_name', 'last_name',
            'email', 'photo', 'has_usable_password', 'id'
        ]


class SearchUserSerializer(serializers.ModelSerializer):
    full_name = serializers.SerializerMethodField()

    def get_full_name(self, obj):
        return obj.full_name

    class Meta:
        model = UserModel
        fields = ['id', 'full_name', 'email']


class UserUpdateSerializer(serializers.ModelSerializer):
    username = serializers.CharField(required=False)

    def validate(self, attrs):
        attrs = super(UserUpdateSerializer, self).validate(attrs)
        user = self.context["request"].user
        if "username" in attrs and user.username != attrs["username"] \
                and UserModel.objects.filter(username=attrs["username"]).exists():
            raise ValidationError(**codes.USERNAME_UNAVAILABLE)
        return attrs

    class Meta:
        model = UserModel
        fields = [
            'id', 'username', 'first_name', 'last_name', 'photo'
        ]


class CheckValidPasswordMixin(serializers.Serializer):

    def __init__(self, *args, **kwargs):
        super(CheckValidPasswordMixin, self).__init__(*args, **kwargs)
        self.request = self.context.get('request', None)
        self.user = getattr(self.request, 'user', None)

    def validate(self, attrs):
        if "password" not in attrs or not self.user.check_password(attrs["password"]):
            raise ValidationError(**codes.INVALID_PASSWORD)
        return attrs


class PasswordSetSerializer(serializers.Serializer):
    """
    Change password serializer
    """
    password = serializers.CharField(
        help_text=_('Contraseña actual'),
        max_length=PASSWORD_MAX_LENGTH,
    )

    confirm_password = serializers.CharField(
        help_text=_('Nueva Contraseña'),
        max_length=PASSWORD_MAX_LENGTH
    )

    def __init__(self, *args, **kwargs):
        super(PasswordSetSerializer, self).__init__(*args, **kwargs)

    def validate(self, attrs):
        attrs = super(PasswordSetSerializer, self).validate(attrs)
        if attrs["password"] != attrs["confirm_password"]:
            raise ValidationError(**codes.PASSWORD_MISTMATCH)
        return attrs

class PasswordUpdateSerializer(CheckValidPasswordMixin):
    """
    Change password serializer
    """
    password = serializers.CharField(
        help_text=_('Contraseña actual'),
        max_length=PASSWORD_MAX_LENGTH,
    )

    password1 = serializers.CharField(
        help_text=_('Nueva Contraseña'),
        max_length=PASSWORD_MAX_LENGTH
    )

    password2 = serializers.CharField(
        help_text=_('Nueva Contraseña'),
        max_length=PASSWORD_MAX_LENGTH
    )

    def __init__(self, *args, **kwargs):
        super(PasswordUpdateSerializer, self).__init__(*args, **kwargs)

    def validate(self, attrs):
        #import ipdb; ipdb.set_trace()
        attrs = super(PasswordUpdateSerializer, self).validate(attrs)
        if attrs["password1"] != attrs["password2"]:
            raise ValidationError(**codes.PASSWORD_MISTMATCH)
        return attrs


class EmailUpdateSerializer(CheckValidPasswordMixin):
    """
    Change email serializer
    """
    password = serializers.CharField(
        help_text=_('Contraseña'),
        max_length=PASSWORD_MAX_LENGTH,
    )

    new_email = serializers.EmailField(
        help_text=_('Nuevo Correo'),
    )

    def validate(self, attrs):
        attrs = super(EmailUpdateSerializer, self).validate(attrs)
        if self.user.email != attrs["new_email"] \
                and UserModel.objects.filter(email=attrs["new_email"]).exists():
            raise ValidationError(**codes.EMAIL_UNAVAILABLE)
        return attrs