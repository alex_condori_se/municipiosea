# -*- encoding:utf-8 -*-

from django.template.loader import render_to_string
from django.urls import reverse

from apps.contrib.email import send_email
from apps.contrib.utils.strings import get_hostname


class UserMessages(object):

    @staticmethod
    def cancel_sent(request):
        path = reverse("users:view-cancel-account")
        hostname = get_hostname(request)
        data = dict()
        data["user"] = request.user
        data["action_url"] = hostname + path
        context = {"request": request, "data": data}

        subject = render_to_string(
            template_name="account/email/cancel.txt",
            context=None
        )
        text_body = render_to_string(
            template_name="account/email/cancel_body.txt",
            context=context
        )
        html_body = render_to_string(
            template_name="account/email/cancel_body.html",
            context=context
        )

        send_email(subject, [request.user.email], text_body, html_body)
