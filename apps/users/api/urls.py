from django.urls import path, include
from apps.users.views import UserUpdateView

app_name = 'users'
urlpatterns = [

    # v.1
    path('/v1', include("apps.users.api.v1.urls", namespace="v1")),

    path('account/update/', UserUpdateView.as_view(), name='view-update'),
]
