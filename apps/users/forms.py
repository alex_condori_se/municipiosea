
import logging

from constance import config
from allauth.account.forms import SignupForm as SignupFormBase

from django import forms
from django.contrib.auth import get_user_model, password_validation
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from apps.users.models import User

UserModel = get_user_model()
logger = logging.getLogger(__name__)


class UserCreationForm(forms.ModelForm):
    error_messages = {
        'password_mismatch': _("Las contraseñas no coinciden"),
    }
    password1 = forms.CharField(
        strip=False,
        widget=forms.PasswordInput,
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        widget=forms.PasswordInput,
        strip=False,
        help_text=_("Ingresa la misma contraseña para la verificación"),
    )

    class Meta:
        model = UserModel
        fields = ["username", "email", "first_name", "last_name", "photo", "is_active", "is_staff"]

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        self.instance.username = self.cleaned_data.get('username')
        password_validation.validate_password(self.cleaned_data.get('password2'), self.instance)
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserForm(forms.ModelForm):
    password2 = forms.CharField()

    def clean(self):
        cleaned_data = super(UserForm, self).clean()
        password = cleaned_data.get("password")
        password2 = cleaned_data.get("password2")

        if password != password2:
            raise forms.ValidationError(
                "Las contraseñas deben ser iguales"
            )

    class Meta:
        model = User
        fields = '__all__'


class SignupForm(SignupFormBase):

    def clean(self):
        data = super(SignupForm, self).clean()

        if not config.REGISTRATION_OPEN:
            raise ValidationError(
                _('El registro de nuevas cuentas esta cerrado por ahora! :('),
                code='closed'
            )

        return data
