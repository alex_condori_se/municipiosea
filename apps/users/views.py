# -*- encoding:utf-8 -*-

from __future__ import absolute_import, unicode_literals

from allauth.account.views import PasswordSetView
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Q
from django.urls import reverse_lazy
from django.shortcuts import redirect, render
from django.utils.safestring import mark_safe
from django.views.generic import TemplateView, ListView, CreateView, DeleteView
from django.views.generic.edit import UpdateView
from django import forms

from django.contrib import messages

from apps.contrib.views import MenuMixin
from apps.users.api.v1.serializers import UserSerializer
from apps.users.forms import UserForm
from apps.users.mixins import UserJSONMixin
from apps.users.models import User
#from apps.events.mixins import IsOrganizationStaffMixin


from allauth.socialaccount.signals import (
    social_account_added, pre_social_login,
    social_account_updated, social_account_removed
)
from django.dispatch import receiver

UserModel = get_user_model()


"""
class ProfileView(LoginRequiredMixin, UserJSONMixin,  MenuMixin, TemplateView):
    #template_name = "admin/users/profile.html"
    template_name = "planwi/account/profile.html"
    menu = "profile"

class DashboardView(LoginRequiredMixin, MenuMixin, TemplateView):
    template_name = "admin/users/dashboard.html"
    menu = "dashboard"
"""

#class ProfileView(LoginRequiredMixin, IsOrganizationStaffMixin, TemplateView):
class ProfileView(TemplateView):

    #template_name = "users/profile.html"
    template_name = "planwi/account/profile.html"
    menu = "profile"

    def post(self, request, *args, **kwargs):
        user = request.user
        form =  UserUpdateForm(instance=user, data=request.POST, files=request.FILES)
        # import ipdb; ipdb.set_trace()

        messages.add_message(request, messages.SUCCESS, 'Datos actualizados con exito.')
        #messages.error(request, 'Ocurrio un error al actualizar los datos.')
        if form.is_valid():
            form.save(commit=True)
            return redirect("users:view-profile")
        else:
            return render(request, template_name=self.template_name, context={"form": form,})


    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        if self.request:# and self.request.user.is_authenticated():
            context["user"] = self.request.user
        else:
            context["user"] = None

        context["menu"] = self.menu
        return context


# class PasswordSetView(TemplateView):
#     pass

# SIGNALS

@receiver(pre_social_login)
def pre_social_login_apply(sender, request, sociallogin, **kwargs):
    print("\n\n\n\n pre_social_login_apply \n\n\n\n")\

@receiver(social_account_added)
def social_account_added_apply(sender, request, sociallogin, **kwargs):
    print("\n\n\n\n social_account_added \n\n\n\n")


@receiver(social_account_updated)
def social_account_updated_apply(sender, request, sociallogin, **kwargs):
    print("\n\n\n\n social_account_updated \n\n\n\n")


@receiver(social_account_removed)
def social_account_removed_apply(sender, request, sociallogin, **kwargs):
    print("\n\n\n\n social_account_removed \n\n\n\n")


class UserUpdateForm(forms.ModelForm):
    #
    # def clean_username(self):
    #     username = self.cleaned_data['username']
    #     try:
    #         user = User.objects.get(username=username)
    #     except User.DoesNotExist:
    #         return username
    #     raise forms.ValidationError(u'%s already exists' % username)

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'photo']


class UserUpdateView(LoginRequiredMixin, UpdateView):
    model = User
    form_class = UserUpdateForm


    def get(self, request, *args, **kwargs):
        import ipdb;ipdb.set_trace()

    def post(self, request, *args, **kwargs):
        import ipdb;ipdb.set_trace()
    #     user =  request.user
    #     form = UserUpdateForm(instance=user)

    def get_success_url(self):
        return reverse('users:profile')

    def get_object(self):
        return self.request.user


class MenuMixin(object):
    def get_context_data(self, **kwargs):
        context = super(MenuMixin, self).get_context_data(**kwargs)
        context["menu"] = self.menu
        return context
