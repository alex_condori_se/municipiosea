from django.contrib.admin.widgets import FilteredSelectMultiple
from django.utils.datastructures import MultiValueDict


class ArrayFieldSelectMultiple(FilteredSelectMultiple):

    def __init__(self, verbose_name, is_stacked=False, *args, **kwargs):
        self.delimiter = kwargs.pop("delimiter", ",")
        super(ArrayFieldSelectMultiple, self).__init__(verbose_name, is_stacked, *args, **kwargs)

    def render(self, name, value, attrs=None, renderer=None):
        if isinstance(value, str):
            value = value.split(self.delimiter)
        return super(ArrayFieldSelectMultiple, self).render(name, value, attrs, renderer)

    def value_from_datadict(self, data, files, name):
        if isinstance(data, MultiValueDict):
            # Normally, we'd want a list here, which is what we get from the
            # SelectMultiple superclass, but the SimpleArrayField expects to
            # get a delimited string, so we're doing a little extra work.
            return self.delimiter.join(data.getlist(name))
        return data.get(name, None)
