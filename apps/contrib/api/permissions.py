# -*- encoding:utf-8 -*-

from rest_framework.permissions import IsAuthenticated


class IsNotAuthenticated(IsAuthenticated):
    """
    Restrict access only to unauthenticated users.
    """
    def has_permission(self, request, view):
        if request.user and request.user.is_authenticated():
            return False
        else:
            return True


class IsSuperUser(IsAuthenticated):
    def has_permission(self, request, view, obj=None):
        return request.user and request.user.is_authenticated() and request.user.is_superuser







