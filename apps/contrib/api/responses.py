# -*- encoding:utf-8 -*-

from django.utils.translation import ugettext_lazy as _

from rest_framework import status as status_code
from rest_framework.response import Response


class DoneResponse(Response):
    """
    Base class for REST Exceptions based on CEH from @vicobits.
    """
    def __init__(self, detail=None, code=None, status=None):
        response = dict()
        response["message"] = detail if detail else _("Operación exitosa !")
        response["code"] = code if code else "successful_action"
        _status = status if status else status_code.HTTP_200_OK
        super(DoneResponse, self).__init__(data=response, status=_status)


class NoContent(Response):
    """
    Base class for REST Exceptions based on CEH from @vicobits.
    """
    def __init__(self, detail=None, code=None, status=None):
        response = dict()
        response["message"] = detail if detail else _("Sin Contenido!")
        response["code"] = code if code else "no_content"
        _status = status if status else status_code.HTTP_204_NO_CONTENT
        super(NoContent, self).__init__(data=response, status=_status)

