
class MenuMixin(object):
    menu = None

    def get_context_data(self, **kwargs):
        context = super(MenuMixin, self).get_context_data(**kwargs)
        context["menu"] = self.menu
        return context
