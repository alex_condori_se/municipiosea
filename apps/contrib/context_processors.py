# -*- encoding: utf-8 -*-
from constance import config
from django.conf import settings

GLOBAL_CONTEXT = {
    'project_domain': settings.PROJECT_DOMAIN,
    'project_name': settings.PROJECT_NAME,
    'project_author': settings.PROJECT_AUTHOR,
    'project_description': settings.PROJECT_DESCRIPTION,
    'project_support_email': settings.PROJECT_SUPPORT_EMAIL,
    'project_support_phone': settings.PROJECT_SUPPORT_PHONE,
    'DEBUG': settings.DEBUG,
    #'social_registration': config.SOCIAL_REGISTRATION_OPEN,
}


def website(request):
    """
    Returns common info about the website
    """
    return GLOBAL_CONTEXT