# -*- coding: utf-8 -*-

import time
import uuid


def timestamp_ms():
    return int(time.time() * 1000)


def gen_pin(digits=6):
    return str(hash(str(uuid.uuid1())) % 10 ** digits)
