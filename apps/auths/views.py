# -*- encoding:utf-8 -*-

from django.shortcuts import render
from django.views import View
from django.utils.translation import ugettext_lazy as _

from apps.auths.forms import ResetPasswordForm
from .models import UserAction
from .messages import AuthMessages


class ResetPasswordView(View):

    def get(self, request, token=None):

        print("\n\n\n\n", token, "\n\n\n\n")
        # import ipdb;ipdb.set_trace()

        context = dict()
        context["token_fail"] = not UserAction.objects.filter(
            token=token, type=UserAction.ACTION_RESET_PASSWORD).exists()

        return render(request, 'account/password_reset_from_key.html', context)

    def post(self, request, token=None):
        context = dict()
        context["title"] = _("Reset Password")
        context["token_fail"] = not UserAction.objects.filter(token=token,
                                                              type=UserAction.ACTION_RESET_PASSWORD).exists()

        if not context["token_fail"]:
            action = UserAction.objects.get(token=token, type=UserAction.ACTION_RESET_PASSWORD)

            user = action.user

            if user.is_active:
                form = ResetPasswordForm(data=request.POST)
                context["form"] = form
                if form.is_valid():
                    password = form.cleaned_data["password1"]

                    user = action.user
                    user.set_password(password)
                    user.save()
                    action.delete()

                    return render(request, 'account/password_reset_from_key_done.html', context)
                else:
                    context["has_error"] = True
                    context["message"] = _("Passwords aren't match!")
                    return render(request, 'account/password_reset_from_key.html', context)

            else:
                context["title"] = _("Cuenta Inactiva")
                context["message"] = _("Su cuenta esta inactiva, porfavor contactese con un administrador "
                                       "a info@planwi.com")
                return render(request, 'account/message.html', context)

        else:
            return render(request, 'account/password_reset_from_key.html', context)


# TODO Cambiar Integrar la Activación con el flujo de Django Allauth
class ConfirmRegisterView(View):

    def get(self, request, token=None):

        context = dict()
        context["title"] = _("Confirmación de Cuenta")

        if UserAction.objects.filter(token=token, type=UserAction.ACTION_ENABLE_ACCOUNT).exists():
            action = UserAction.objects.get(token=token, type=UserAction.ACTION_ENABLE_ACCOUNT)

            user = action.user
            user.is_active = True
            user.save()

            AuthMessages.send_welcome(request, action)
            action.delete()

            context["message"] = _("Tu cuenta ha sido activada exitosamente.")
            return render(request, 'account/message.html', context)

        else:
            context["message"] = _("Esta Cuenta es inválida o ya fué activada.")
            context["error"] = True
            return render(request, 'account/message.html', context)
