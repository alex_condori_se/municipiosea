# -*- encoding:utf-8 -*-

from django.urls import path

from .views import ResetPasswordView, ConfirmRegisterView

urlpatterns = [

    # TRANSACTIONS
    path('account/activation/<slug:token>/',
        ConfirmRegisterView.as_view(),
        name="account-activation",
    ),

    path(
        'password/reset/<slug:token>/',
        view=ResetPasswordView.as_view(),
        name="reset-password",
    ),
]



