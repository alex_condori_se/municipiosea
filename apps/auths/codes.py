# -*- encoding:utf-8 -*-

from django.utils.translation import ugettext_lazy as _
from rest_framework import status


# ~ ERRORS
# --------------------------------------------

CREDENTIALS_REQUIRED = {
    "code": "credentials_required",
    "detail": _("El correo/usuario y contraseña son requeridos"),
}

INACTIVE_ACCOUNT = {
    "code": "inactive_account",
    "detail": _("La cuenta no esta activa"),
}

UNVERIFIED_EMAIL = {
    "code": "unverified_email",
    "detail": _("El correo electrónico debe ser verificado"),
}

INVALID_CREDENTIALS = {
    "code": "invalid_credentials",
    "detail": _("Credenciales usuario/correo y contraseña son inválidas"),
}

INVALID_TOKEN = {
    "code": "invalid_token",
    "detail": _("Es TOKEN no es invalido o esta EXPIRADO"),
}

USER_NOT_FOUND = {
    "code": "user_not_found",
    "detail": _("Usuario no encontrado"),
}

PASSWORD_MISTMATCH = {
    "code": "password_mistmatch",
    "detail": _("Las contraseñas no coinciden"),
}

UNAVAILABLE_EMAIL = {
    "code": "unavailable_email",
    "detail": _("El correo electrǿnico ya esta siendo utilizado."),
}


# ~ SUCCESS
# --------------------------------------------
RESET_PASSWORD_SENT = {
    "code": "reset_password_sent",
    "detail": _("Un correo para restaurar contraseña fué enviado"),
    "status": status.HTTP_200_OK
}

PASSWORD_UPDATED = {
    "code": "password_updated",
    "detail": _("Contraseña Actualizada"),
    "status": status.HTTP_200_OK
}

LOGGED_OUT = {
    "code": "logged_out",
    "detail": _("Se ha cerrado sesión exitosamente."),
    "status": status.HTTP_200_OK
}

CONFIRMATION_EMAIL_SENT = {
    "code": "confirmation_email_sent",
    "detail": _("Confirmación de correo electrónico enviado"),
    "status": status.HTTP_200_OK
}


EMAIL_CONFIRMED = {
    "code": "email_confirmed",
    "detail": _("Correo Electrónico confirmado"),
    "status": status.HTTP_200_OK
}
