# -*- encoding:utf-8 -*-

from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import ugettext_lazy as _

UserModel = get_user_model()


class UserAction(models.Model):

    ACTION_ENABLE_ACCOUNT = "ENABLE_ACCOUNT"
    ACTION_RESET_PASSWORD = "RESET_PASSWORD"
    ACTION_DELETE_ACCOUNT = "DELETE_ACCOUNT"

    ACTION_TYPE = (
        (ACTION_ENABLE_ACCOUNT, _("Habilitar Cuenta")),
        (ACTION_RESET_PASSWORD, _("Restaurar Contraseña")),
        (ACTION_DELETE_ACCOUNT, _("Eliminar Cuenta")),
    )

    user = models.ForeignKey(
        UserModel, verbose_name=_("Usuario"),
        related_name="actions",
        on_delete=models.CASCADE
    )
    type = models.CharField(max_length=50, choices=ACTION_TYPE, db_index=True)

    creation_date = models.DateTimeField(auto_now=True)
    expiration_date = models.DateTimeField(db_index=True,)
    token = models.CharField(max_length=150, db_index=True)

    class Meta:
        verbose_name = _('Acción')
        verbose_name_plural = _('Acciones')