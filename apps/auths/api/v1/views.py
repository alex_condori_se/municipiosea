# -*- encoding:utf-8 -*-

from allauth.account.adapter import get_adapter
from allauth.account.forms import EmailAwarePasswordResetTokenGenerator
from allauth.account.models import EmailConfirmationHMAC
from allauth.account.utils import complete_signup, user_pk_to_url_str, user_username
from allauth.socialaccount.models import SocialAccount
from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from allauth.socialaccount.providers.oauth2.client import OAuth2Client
from allauth.utils import build_absolute_uri
from django.contrib.auth.signals import user_logged_in
from django.contrib.auth import (
    login as django_login,
)
from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.debug import sensitive_post_parameters
from rest_framework import status

from rest_framework.authtoken.models import Token as DefaultTokenModel


from rest_framework.generics import GenericAPIView, ListAPIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.viewsets import ViewSet

from allauth.account import app_settings as allauth_settings
from allauth.socialaccount.adapter import get_adapter as get_social_adapter
from allauth.socialaccount import signals

from apps.auths.api.v1.social_serializers import SocialLoginSerializer, SocialConnectSerializer, SocialAccountSerializer
from apps.auths.api.v1.utils import import_callable, default_create_token
from apps.auths.messages import AuthMessages
from apps.auths import codes
from apps.contrib.api.exceptions import NotFound
from apps.contrib.api.responses import DoneResponse
from apps.users.api.v1.serializers import UserSerializer

from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter


from .serializers import (
    LoginSerializer, PasswordResetSerializer,
    PasswordResetConfirmSerializer, RegisterSerializer,
    VerifyEmailSerializer, TokenSerializer
)

sensitive_post_parameters_m = method_decorator(
    sensitive_post_parameters(
        'password', 'old_password', 'new_password1', 'new_password2'
    )
)

TokenModel = import_callable(getattr(settings, 'REST_AUTH_TOKEN_MODEL', DefaultTokenModel))
create_token = import_callable(getattr(settings, 'REST_AUTH_TOKEN_CREATOR', default_create_token))


def get_session(token, user):
    return Response({
        'token': token.key,
        'profile': UserSerializer(user).data,
    }, status=status.HTTP_200_OK)

class TokenAuthViewSet(ViewSet):
    authentication_classes = ()
    permission_classes = [AllowAny]

    def register(self, request):
        serializer = RegisterSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save(self.request)
        token, created = TokenModel.objects.get_or_create(user=user)
        complete_signup(self.request._request,
                        user, allauth_settings.EMAIL_VERIFICATION, None)
        if allauth_settings.EMAIL_VERIFICATION == allauth_settings.EmailVerificationMethod.MANDATORY:
            return DoneResponse(**codes.CONFIRMATION_EMAIL_SENT)
        return get_session(token, user)

    def confirm_email(self, request):
        serializer = VerifyEmailSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        confirmation = EmailConfirmationHMAC.from_key(serializer.validated_data['key'])
        confirmation.confirm(self.request)
        return DoneResponse(**codes.EMAIL_CONFIRMED)

    def login(self, request):
        """
        Inicio de Sesión
        """
        serializer = LoginSerializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)

        user = serializer.validated_data['user']
        token, created = TokenModel.objects.get_or_create(user=user)
        print("*************def login***************")
        user_logged_in.send(sender=user.__class__, request=request, user=user)

        if hasattr(settings, "AUTH_TOKEN_SESSION") and settings.AUTH_TOKEN_SESSION:
            django_login(self.request, user)
        print(token, user)
        return get_session(token, user)

    def password_reset(self, request):
        """
        Restaurar Contraseña
        """
        serializer = PasswordResetSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        action = serializer.validated_data["action"]

        if "redirect_uri" in serializer.validated_data:
            redirect_uri = serializer.validated_data["redirect_uri"]
            AuthMessages.reset_password(request, action, redirect_uri=redirect_uri)
        else:
            self.get_allauth_reset_password(action.user)

        return DoneResponse(**codes.RESET_PASSWORD_SENT)


    def get_allauth_reset_password(self, user):
        token_generator = EmailAwarePasswordResetTokenGenerator()
        temp_key = token_generator.make_token(user)


        # send the password reset email
        path = reverse("account_reset_password_from_key", kwargs=dict(uidb36=user_pk_to_url_str(user), key=temp_key))
        url = build_absolute_uri(self.request, path)

        context = {"current_site": get_current_site(self.request),
                   "user": user, "password_reset_url": url, "request": self.request}

        if settings.ACCOUNT_AUTHENTICATION_METHOD != 'email':
            context['username'] = user_username(user)
        get_adapter(self.request).send_mail('account/email/password_reset_key',user.email, context)

    def password_reset_confirm(self, request):
        """
        Confirmar la Restauración de Contraseña
        """
        serializer = PasswordResetConfirmSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        password = serializer.validated_data["password"]
        action = serializer.validated_data["action"]

        action.user.set_password(password)
        action.delete()
        return DoneResponse(**codes.PASSWORD_UPDATED)

    def logout(self, request):
        """
        Clear all application sessions.
        """
        serializer = TokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        token = serializer.validated_data["token"]
        token.delete()
        return DoneResponse(**codes.LOGGED_OUT)




class LoginView(GenericAPIView):
    """
    Check the credentials and return the REST Token
    if the credentials are valid and authenticated.
    Calls Django Auth login method to register User ID
    in Django session framework

    Accept the following POST parameters: username, password
    Return the REST Framework Token Object's key.
    print("*****************loginView - post**********************")
    """
    permission_classes = [AllowAny]
    serializer_class = LoginSerializer
    token_model = TokenModel

    user = None
    token = None
    request = None
    serializer = None

    @sensitive_post_parameters_m
    def dispatch(self, *args, **kwargs):
        return super(LoginView, self).dispatch(*args, **kwargs)

    def process_login(self):
        django_login(self.request, self.user)

    def login(self):
        self.user = self.serializer.validated_data['user']
        self.token = create_token(self.token_model, self.user)
        #
        # if getattr(settings, 'REST_SESSION_LOGIN', True):
        #     self.process_login()

    def get_response(self):
        return get_session(self.token, self.user)


    def post(self, request, *args, **kwargs):
        #print("*****************loginView - post**********************")
        self.request = request
        self.serializer = self.get_serializer(data=self.request.data, context={'request': request})
        self.serializer.is_valid(raise_exception=True)
        self.login()
        return self.get_response()


class SocialLoginView(LoginView):
    """
    class used for social authentications
    example usage for facebook with access_token
    -------------
    from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter

    class FacebookLogin(SocialLoginView):
        adapter_class = FacebookOAuth2Adapter
    -------------

    example usage for facebook with code

    -------------
    from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
    from allauth.socialaccount.providers.oauth2.client import OAuth2Client

    class FacebookLogin(SocialLoginView):
        adapter_class = FacebookOAuth2Adapter
        client_class = OAuth2Client
        callback_url = 'localhost:8000'
    -------------
    """
    serializer_class = SocialLoginSerializer

    def process_login(self):
        get_adapter(self.request).login(self.request, self.user)


class SocialConnectView(LoginView):
    """
    class used for social account linking

    example usage for facebook with access_token
    -------------
    from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter

    class FacebookConnect(SocialConnectView):
        adapter_class = FacebookOAuth2Adapter
    -------------
    """
    serializer_class = SocialConnectSerializer
    permission_classes = (IsAuthenticated,)

    def process_login(self):
        get_adapter(self.request).login(self.request, self.user)


class SocialAccountListView(ListAPIView):
    """
    List SocialAccounts for the currently logged in user
    """
    serializer_class = SocialAccountSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return SocialAccount.objects.filter(user=self.request.user)


class SocialAccountDisconnectView(GenericAPIView):
    """
    Disconnect SocialAccount from remote service for
    the currently logged in user
    """
    serializer_class = SocialConnectSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return SocialAccount.objects.filter(user=self.request.user)

    def post(self, request, *args, **kwargs):
        accounts = self.get_queryset()
        account = accounts.filter(pk=kwargs['pk']).first()
        if not account:
            raise NotFound

        get_social_adapter(self.request).validate_disconnect(account, accounts)

        account.delete()
        signals.social_account_removed.send(
            sender=SocialAccount,
            request=self.request,
            socialaccount=account
        )

        return Response(self.get_serializer(account).data)


class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter


class GoogleLogin(SocialLoginView):
    adapter_class = GoogleOAuth2Adapter
    client_class = OAuth2Client
    callback_url = settings.PROJECT_DOMAIN + "/account/google/login/callback/"
    redirect_uri = settings.PROJECT_DOMAIN
