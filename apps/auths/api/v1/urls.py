# -*- encoding:utf-8 -*-

from django.urls import path


from .views import TokenAuthViewSet, FacebookLogin, GoogleLogin

app_name = 'auths'

urlpatterns = [

    path('/register/', TokenAuthViewSet.as_view({
        "post": "register"}), name='register'),

    path('/email/confirmation/', TokenAuthViewSet.as_view({
        "post": "confirm_email"}), name='email-confirmation'),

    path('/login/', TokenAuthViewSet.as_view({
        "post": "login"}), name='login'),

    path('/password/reset/', TokenAuthViewSet.as_view({
        "post": "password_reset"}), name='password-reset'),

    path('/password/reset/confirm/', TokenAuthViewSet.as_view({
        "post": "password_reset_confirm"}), name='password-reset-confirm'),

    path('/logout/', TokenAuthViewSet.as_view({"post": "logout"}), name='logout'),

    path('/facebook/login/', FacebookLogin.as_view(), name='facebook-login'),
    path('/google/login/', GoogleLogin.as_view(), name='google-login')
]
