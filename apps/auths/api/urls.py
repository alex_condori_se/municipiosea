# -*- encoding:utf-8 -*-

from django.urls import path, include

app_name = 'auths'
urlpatterns = [

    # API v.1
    path('/v1/auth', include("apps.auths.api.v1.urls", namespace="v1")),

]



