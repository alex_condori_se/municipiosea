## Planwi

Sitio web para registrar eventos socio culturales.


### Technologies

  * [Django](https://www.djangoproject.com/)
  * [Django Rest Framework](http://www.django-rest-framework.org/)
  

## Funcionalidades

  * Servicios web para aplicaciones móviles `inicio de sesión`, `registro` y `restaurar contraseña`
  * Autenticación via Token con `knox`
  * Soporte para `docker` para desarrollo y producción
  * Plantillas bsicas `Perfil de usuario`, `Panel de control`, `autenticación`, `registro` y `restaurar contraseña`

## Para desarrollo

Para desarrollar necesita tener instalado `git`, `docker`, `llaves ssh` y una `terminal decente`.

#### Comandos básicos
  * `make build` Construye las imagenes docker que necesita el proyecto. 
  * `make server` Instancia los contenedores con el proyecto y ejecuta el servidor de django de desarrollo.
  * `make django` Ejecuta el servidor de django en modo `debug`.
  * `make migrations` Crear migraciones con django
  * `make migrate` Aplica las migraciones con django
  * `make superuser` Crea un super usuario en django, para sistemas windows es posible que tenga que escribir `winpty make superuser`, dependiendo de la terminal que utiliza.

#### Frontend
  * `npm install` Instala las dependencias frontend para compilar los archivos staticos.
  * `gulp bundle` Compila y  empaqueta los archivos `css` y `js` para desarrollo.
  * `gulp bundle --prod` Compila y  empaqueta los archivos `css` y `js` para producción.
  * `gulp watch` Modo escucha para compilación continua.
  
### Configurar PyCharm (MacOS)

  * `brew install socat` Instala socat, es una app que convierte un un archivo socker en un puerto.
  * `socat -d -d TCP-L:8099,fork UNIX:/var/run/docker.sock` ejecuta docker en el puerto `8099`.
  * Configurar pycharm API URL como: `tcp://localhost:8099` sin certificados de seguridad.


## Para producción
 * Por completar
  

## Commercial Support

`Gecko` es mantenido por el equipo [@xiberty](http://xiberty.com), para más información escribanos a: **support@xiberty.com**
